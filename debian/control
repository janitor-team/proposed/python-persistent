Source: python-persistent
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Barry Warsaw <barry@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 10),
               dh-python,
               python3-all-dev,
               python3-repoze.sphinx.autointerface,
               python3-setuptools,
               python3-sphinx,
               python3-zope.interface
Standards-Version: 3.9.8
Homepage: https://pypi.python.org/pypi/persistent/
Vcs-Git: https://salsa.debian.org/python-team/modules/python-persistent.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-persistent

Package: python3-persistent
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Suggests: python-persistent-doc
Description: Automatic persistence for Python objects
 This package contains a generic persistence implementation for Python. It
 forms the core protocol for making objects interact "transparently" with
 a database such as the ZODB.
 .
 This is the Python 3 version.

Package: python-persistent-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Automatic persistence for Python objects - documentation
 This package contains a generic persistence implementation for Python. It
 forms the core protocol for making objects interact "transparently" with
 a database such as the ZODB.
 .
 This package contains the Python module documentation. Alternatively,
 there is an online version at https://persistent.readthedocs.io/
